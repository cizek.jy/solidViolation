package cz.cizek.solid.violation;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cizekj on 14.01.16.
 */
public class Worker {

    private int manDayRate;
    private List<VehicleType> acceptableVehicles = new ArrayList<VehicleType>();

    public Worker(VehicleType... vehicleTypes) {

        for (VehicleType vehicleType : vehicleTypes) {
            acceptableVehicles.add(vehicleType);
        }
    }

    public boolean isCarAcceptable() {
        return acceptableVehicles.contains(VehicleType.CAR);
    }

    public boolean isLorryAcceptable() {
        return acceptableVehicles.contains(VehicleType.LORRY);
    }

    public boolean isMotorbikAcceptable() {
        return acceptableVehicles.contains(VehicleType.MOTORBIKE);
    }

    public int getManDayRate() {

        return manDayRate;
    }

    public void setManDayRate(int manDayRate) {

        this.manDayRate = manDayRate;
    }
}
