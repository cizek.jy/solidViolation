package cz.cizek.solid.violation;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cizekj on 14.01.16.
 */
public class ReservationService implements Service{

    private List<Garage> garagePool;
    private List<Worker> workerPool;
    private PersistenceService persistenceService;

    public ReservationService(PersistenceService persistenceService) {

        this.persistenceService = persistenceService;

        garagePool = new ArrayList<Garage>() {
            {
                //init garagePool
            }
        };

        workerPool = new ArrayList<Worker>() {
            {
                //init garagePool
            }
        };
    }

    @Override
    public boolean makeReservation(Car car) {

        if (car instanceof Lorry) {
            for (Garage garage : garagePool) {
                if (garage.isCarAcceptable()) {
                    getGarageFromPool(garage);
                    for (Worker worker : workerPool) {
                        if (worker.isCarAcceptable()) {
                            getWorkerFromPool(worker);
                            persistenceService.saveReservation(garage, worker, car);
                            return true;
                        }
                    }
                }
            }
            return false;
        }
        else if (car instanceof Car) {
            for (Garage garage : garagePool) {
                if (garage.isLorryAcceptable()) {
                    getGarageFromPool(garage);
                    for (Worker worker : workerPool) {
                        if (worker.isCarAcceptable()) {
                            getWorkerFromPool(worker);
                            persistenceService.saveReservation(garage, worker, car);
                            return true;
                        }
                    }
                }
            }
            return false;
        }
        return false;
    }

    @Override
    public boolean makeReservation(Motorbike motorbike) {

        for (Garage garage : garagePool) {
            if (garage.isMotorbikAcceptable()) {
                getGarageFromPool(garage);
                for (Worker worker : workerPool) {
                    if (worker.isCarAcceptable()) {
                        getWorkerFromPool(worker);
                        persistenceService.saveReservation(garage, worker, motorbike);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override
    public void workDone(Garage garage, Worker worker) {

        putGarageToPool(garage);
        putWorkerToPool(worker);
    }

    @Override
    public void cancelReservation(Garage garage, Worker worker) {

        putGarageToPool(garage);
        putWorkerToPool(worker);
    }

    @Override
    public void saveReservation(Garage garage, Worker worker, Car car) {

    }

    @Override
    public void saveReservation(Garage garage, Worker worker, Motorbike motorbike) {

    }

    private void getGarageFromPool(Garage garage) {
        //get garage...
    }

    private void putGarageToPool(Garage garage) {
        //put free garage to pool...
    }

    private void getWorkerFromPool(Worker worker) {
        //get worker...
    }

    private void putWorkerToPool(Worker worker) {
        //put free worker to pool...
    }
}
