package cz.cizek.solid.violation;

/**
 * Created by cizekj on 14.01.16.
 */
public class PersistenceService implements Service {

    @Override
    public boolean makeReservation(Car car) {

        return false;
    }

    @Override
    public boolean makeReservation(Motorbike motorbike) {

        return false;
    }

    @Override
    public void workDone(Garage garage, Worker worker) {

    }

    @Override
    public void cancelReservation(Garage garage, Worker worker) {

    }

    @Override
    public void saveReservation(Garage garage, Worker worker, Car car) {
        //persist rezervace
    }

    @Override
    public void saveReservation(Garage garage, Worker worker, Motorbike motorbike) {
        //persist rezervace
    }
}
