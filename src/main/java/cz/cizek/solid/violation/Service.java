package cz.cizek.solid.violation;

/**
 * Created by cizekj on 14.01.16.
 */
public interface Service {

    public boolean makeReservation(Car car);

    public boolean makeReservation(Motorbike motorbike);

    public void workDone(Garage garage, Worker worker);

    public void cancelReservation(Garage garage, Worker worker);

    public void saveReservation(Garage garage, Worker worker, Car car);

    public void saveReservation(Garage garage, Worker worker, Motorbike motorbike);
}
